from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, validators, ValidationError
from wtforms.validators import InputRequired, Email
from wtforms.fields.html5 import EmailField

class Registro(FlaskForm):
    name = StringField(u'Name', [validators.input_required('Name required')])
    last_name = StringField(u'Last Name', [validators.input_required('Last name required')])
    email = EmailField(u'Email', [validators.input_required('Email required'), validators.Email('Email not valid')])
    submit = SubmitField('Guardar')